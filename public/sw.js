'use strict';

self.addEventListener('push', function(event) 
{
    const jsonData = JSON.parse(event.data.text());
    self.registration.showNotification(jsonData.title, jsonData);
});

self.addEventListener('notificationclick', function(event) 
{
    let url = "/";
    const noti = event.notification;
    if (noti.data && noti.data.url) {
        url = noti.data.url;
    }
    event.notification.close();
    
    event.waitUntil(clients.matchAll({
      type: "window"
    }).then(function(clientList) {
      for (var i = 0; i < clientList.length; i++) {
        var client = clientList[i];

        if (client.url == url && 'focus' in client)
          return client.focus();
      }
      
      if (clients.openWindow) {
        return clients.openWindow(url);
      }
    }));
});

self.addEventListener('message', function(event)
{
    const messageData = event.data;
    if(messageData.type == "push") {
        self.registration.showNotification(messageData.data, messageData.data);
    }
});